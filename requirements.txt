argparse==1.3.0
Flask==0.10.1
flask-mongoengine==0.7.1
Flask-WTF==0.12
itsdangerous==0.24
Jinja2==2.8
MarkupSafe==0.23
mongoengine==0.10.0
ordereddict==1.1
pymongo==2.7.1
Werkzeug==0.10.4
wheel==0.24.0
WTForms==2.0.2
