##### All "default" config options above this #####


#MongoDB Config
MONGODB_DB = 'dqm'
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017


try:
    from secret_key import SECRET_KEY
except ImportError:
    # Create missing secret key
    import os
    SECRET_KEY = str(os.urandom(24))
    with open('secret_key.py', 'w+') as key_file:
        key_file.write('# -*- coding: utf-8 -*-\n')
        key_file.write("SECRET_KEY = '{0}'".format(SECRET_KEY))
        key_file.flush()


# Security stuff
# AKA DONT TOUCH
SECURITY_PASSWORD_HASH = 'bcrypt'
SECURITY_PASSWORD_SALT = 'googogogdlsldmbmksmnknvjsndkjfskjtkj'


try:
    from local_settings import *
except ImportError:
    pass