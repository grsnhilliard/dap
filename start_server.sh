#!/usr/bin/env bash
workon dap
gunicorn --bind 0.0.0.0:8000 -w 4 wsgi
