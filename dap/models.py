from mongoengine import Document, StringField, IntField, ListField, ObjectIdField, DictField


# GENERAL DATABASE MODEL CLASSES

class Filter(Document):
    meta = {'collection': 'filters'}
    name = StringField()
    screen = DictField()
    entry = ObjectIdField()


class Run(Document):
    meta = {'collection': 'runs'}
    name = StringField(max_length=80)
    events = ListField(ObjectIdField())
    subRuns = ListField(ObjectIdField(default=None))


class Entry(Document):
    meta = {'collection': 'entries'}
    name = StringField()
    run = StringField()
    histograms = ListField(ObjectIdField(default=None))
    type = StringField()


class Histogram(Document):
    meta = {'collection': 'histograms'}
    filename = StringField()
    run = StringField()
    entry = StringField()
    fullPath = StringField()
    caption = StringField()
    tags = DictField()


class Config(Document):
    meta = {'collection': 'config'}
    ref_run = StringField()
    ref_entry = StringField()
    password = StringField()
    step = IntField()
    added = DictField()


class Caption(Document):
    meta = {'collection': 'captions'}
    search_tring = StringField()
    text = StringField()
