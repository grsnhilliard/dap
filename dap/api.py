from flask import send_file, jsonify, make_response, request
from dap import app, db, dbName
from dap.models import Histogram, Run, Entry, Filter, Config
from mongoengine.errors import *
from bson.objectid import ObjectId
from c3 import start


@app.route('/api/hist/image/<string:id>')
def serve_hist_image(id):
    try:
        hist = Histogram.objects(pk=ObjectId(id)).first()
        return send_file(hist.fullPath, as_attachment=True, mimetype='image/gif')
    except ValueError as e:
        print e

    except LookUpError as e:
        print e


@app.route('/api/runs')
def get_runs():
    run_list = dict()

    run_list['runs'] = [{'name': item.name,
                         'id': str(item.id),
                         'events': [Entry.objects(pk=event).first().name for event in item.events],
                         'subRuns': [Entry.objects(pk=subRun).first().name for subRun in item.subRuns]} for item in
                        Run.objects()]

    return jsonify(run_list)


@app.route('/api/runs/<string:runName>')
def get_run_data(runName):
    run_object = Run.objects(name=runName).first()
    run_dict = {}

    if len(run_object.events) > 0:
        run_dict['events'] = [{'name': Entry.objects(pk=event).first().name,
                               'id': str(event)} for event in run_object.events]

    if len(run_object.subRuns) > 0:
        run_dict['subRuns'] = []
        for subRun in run_object.subRuns:
            # This is ugly as and needs a rewrite.
            try:
                # for item in SubRun.objects(pk=subRun).first(): print item
                name = Entry.objects(pk=str(subRun)).first().name
                run_dict['subRuns'].append({'name': name, 'id': str(subRun)})
            except Exception as e:
                print subRun, e

    return jsonify(run_dict)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route("/api/filters/<string:runName>/<string:entry>")
def get_filters(runName, entry):
    try:
        entry_id = Entry.objects(run=runName, name=entry).first().id
        # print entry_id
    except ValueError as e:
        print e

    try:

        filterDict = Filter.objects(entry=entry_id).first().screen
    except ValueError as e:
        print e

    filter_dict = {'filters': filterDict}
    return jsonify(filter_dict)


@app.route("/api/hists/<string:runName>/<string:entry>")
def gen_hist_list(runName, entry):
    hist_dict = {'hists': []}
    try:
        item_list = Entry.objects(run=runName, name=entry).first().histograms

    except ValueError as e:
        print e

    if item_list:
        for item in item_list:
            hist_object = Histogram.objects(pk=item).first().to_mongo()
            hist_object['_id'] = str(hist_object['_id'])
            # temp = hist_object['tags']
            # hist_object['tags'] = {}
            # for tag_id in temp:
            #     tag_object = Tag.objects(pk=tag_id).first()
            #     hist_object['tags'][tag_object.category] = tag_object.value
            hist_dict['hists'].append(hist_object)

    return jsonify(hist_dict)


@app.route("/api/refhists/<string:runName>/<string:entry>")
def gen_ref_hist_list(runName, entry):
    hist_dict = {'hists': []}
    try:
        item_list = Entry.objects(run=runName, name=entry).first().histograms

    except ValueError as e:
        print e

    if item_list:
        for item in item_list:
            hist_object = Histogram.objects(pk=item).first().to_mongo()
            hist_object['_id'] = str(hist_object['_id'])
            # temp = hist_object['tags']
            # hist_object['tags'] = {}
            # for tag_id in temp:
            #     tag_object = Tag.objects(pk=tag_id).first()
            #     hist_object['tags'][tag_object.category] = tag_object.value
            hist_dict['hists'].append(hist_object)

    return jsonify(hist_dict)


@app.route("/api/refhists/")
def gen_def_ref_hist_list():
    hist_dict = {'hists': []}

    try:
        config_object = Config.objects().first()

    except Exception as e:
        print e

    if not config_object:
        try:
            run_object = Run.objects().first()
            entry_object = Entry.objects(pk=run_object.subRuns[0]).first()
            config_object = Config.objects(ref_entry=entry_object.name).modify(upsert=True, new=True,
                                                                               ref_run=run_object.name)
        except Exception as e:
            print e
    try:

        item_list = Entry.objects(run=config_object.ref_run, name=config_object.ref_entry).first().histograms

    except ValueError as e:
        print e

    if item_list:
        for item in item_list:
            hist_object = Histogram.objects(pk=item).first().to_mongo()
            hist_object['_id'] = str(hist_object['_id'])
            # temp = hist_object['tags']
            # hist_object['tags'] = {}
            # for tag_id in temp:
            #     tag_object = Tag.objects(pk=tag_id).first()
            #     hist_object['tags'][tag_object.category] = tag_object.value
            hist_dict['hists'].append(hist_object)

    return jsonify(hist_dict)


@app.route("/api/reference/<string:runName>/<string:entry>")
def update_reference(runName, entry):
    config_object = Config.objects().first()
    config_object['ref_run'] = runName
    config_object['ref_entry'] = entry
    config_object.save()
    return jsonify({'updateStatus': None})


@app.route("/api/drop")
def dropDB():
    db.connection.drop_database(dbName)
    start(False)
    return jsonify({'dropStatus': None})


@app.route("/api/update")
def updateDB():
    start(False)
    return jsonify({'dropStatus': None})


@app.route("/api/admin", methods=['POST', 'GET'])
def checkPassword():
    if request.data == Config.objects().first().password:
        return jsonify({'admin': 'true'})
    else:
        return jsonify({'admin': 'false'})
