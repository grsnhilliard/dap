from dap import app
from dap import views


app.add_url_rule('/',
                 'alpha',
                 view_func=views.alpha,
                 methods=['GET'])
