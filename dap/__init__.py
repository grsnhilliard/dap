__author__ = 'wghilliard'
from flask import Flask
from flask.ext.mongoengine import MongoEngine
from settings import MONGODB_DB

# Initial Flask Setup
app = Flask('dap')
app.config.from_object('settings')

# Connecting to MongoDB using 'settings.py'.
db = MongoEngine(app)
dbName = MONGODB_DB

import urls
import api
