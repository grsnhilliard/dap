/**
 * Created by wghilliard on 8/30/15.
 */
angular.module('dqmApp')
    .controller('TestCtrl', function ($http, $interval, $modal, $route, $scope, $window, $location, RunpoolService) {
        $scope.model = {};
        $scope.sliderValue = 1;
        $scope.sliderValues = ['Left', 'Both', 'Right'];


        $interval(function () {
            $scope.leftSizer = {'width': (100 / $scope.leftSlider) + '%'};
            $scope.rightSizer = {'width': (100 / $scope.rightSlider) + '%'};
            $scope.applyFilters();
        }, 750);


        $scope.toggleLeft = function () {
            if ($scope.model.showLeft === true) {
                $scope.model.showLeft = false;
            } else {
                $scope.model.showLeft = true;
            }
        };


        $scope.toggleRight = function () {
            if ($scope.model.showRight === true) {
                $scope.model.showRight = false;
            } else {
                $scope.model.showRight = true;
            }
        };


        $scope.toggleEtc = function () {
            if ($scope.model.showEtc === true) {
                $scope.model.showEtc = false;
            } else {
                $scope.model.showEtc = true;
            }
        };


//        -------------------------------------------------------------------------------------
        $scope.fetchRuns = function () {
            RunpoolService.fetchRuns().then(function (data) {
                $scope.model.runs = data.runs;
            });
        };


        $scope.newTab = function () {
            $window.open('http://lbnegpvm01.fnal.gov:5000/#/test', '_blank');

        };


        $scope.leftReset = function () {
            $scope.model.showLeft = true;
            $scope.model.leftStage = 'select';
            $scope.model.leftRunName = '';
            $scope.model.leftEntry = '';
            $scope.model.leftSearch = '';
        };


        $scope.rightReset = function () {
            $scope.model.showRight = true;
            $scope.model.rightStage = 'select';
            $scope.model.rightRunName = '';
            $scope.model.rightEntry = '';
            $scope.model.rightSearch = '';
        };


        $scope.hardReset = function () {
            $scope.model = {
                filterValues: {},
                admin: '',
                adminOn: false
            };
            $scope.leftReset();
            $scope.rightReset();
            $scope.fetchRuns();
        };


        $scope.softReset = function () {
            $scope.model.filterValues = {};
            $scope.applyFilters();
            $scope.leftReset();
            $scope.rightReset();
            $scope.model.filters = null;
        };


        $scope.leftStageOne = function (runName, entry) {
            $scope.model.leftRunName = runName;
            $scope.model.leftEntry = entry;
            $scope.fetchLeftHists(runName, entry);
            $scope.fetchFilters(runName, entry);
            $scope.model.leftStage = 'view';
        };


        $scope.rightStageOne = function (runName, entry) {
            $scope.model.rightRunName = runName;
            $scope.model.rightEntry = entry;
            $scope.fetchRightHists(runName, entry);
            $scope.fetchFilters(runName, entry);
            $scope.model.rightStage = 'view';
        };


        $scope.fetchLeftHists = function (runName, entry) {
            RunpoolService.fetchHists(runName, entry).then(function (data) {
                $scope.model.leftHists = data.hists;
                $scope.model.leftNew = data.hists;
                //console.log($scope.model.leftNew);

            });
        };

        $scope.fetchRightHists = function (runName, entry) {
            RunpoolService.fetchHists(runName, entry).then(function (data) {
                $scope.model.rightHists = data.hists;
                $scope.model.rightNew = data.hists;
                //console.log($scope.model.rightNew);

            });
        };


        $scope.fetchFilters = function (runName, entry) {
            RunpoolService.fetchFilters(runName, entry).then(function (data) {
                if ($scope.model.filters != undefined){

                    console.log('before', $scope.model.filters);
                    for (var thing in data.filters){
                        $scope.model.filters[thing] = _.union(data.filters[thing], $scope.model.filters[thing]);
                    }
                    //$scope.model.filters = _.union(data.filters, $scope.model.filters);
                    console.log('after', $scope.model.filters);
                }else{
                    console.log('data', data.filters);
                    $scope.model.filters = data.filters;
                    console.log('created', $scope.model.filters);
                }

            });
        };


        $scope.applyFilters = function () {
            $scope.model.rightNew = [];
            $scope.model.leftNew = [];

            //console.log($scope.model.filterValues);
            //console.log($scope.model.filterValues.length);
            if (_.isEmpty($scope.model.filterValues)) {
                $scope.model.rightNew = $scope.model.rightHists;
                $scope.model.leftNew = $scope.model.leftHists;
                return;
            }

            for (var category in $scope.model.filterValues) {
                if ($scope.model.filterValues[category].length === 0) {
                    delete $scope.model.filterValues[category];
                }
            }

            for (var index in $scope.model.leftHists) {
                var histogram = $scope.model.leftHists[index];
                var flag = true;
                for (var category in $scope.model.filterValues) {
                    if (histogram.tags[category]) {
                        for (var index2 in $scope.model.filterValues[category]) {
                            if (histogram.tags[category] === $scope.model.filterValues[category][index2]) {
                                $scope.model.leftNew.push(histogram);
                            }
                        }
                    }
                }
            }


            for (index in $scope.model.rightHists) {
                histogram = $scope.model.rightHists[index];
                flag = true;
                for (category in $scope.model.filterValues) {
                    if (histogram.tags[category]) {
                        for (var index2 in $scope.model.filterValues[category]) {
                            if (histogram.tags[category] === $scope.model.filterValues[category][index2]) {
                                $scope.model.rightNew.push(histogram);
                            }
                        }
                    }
                }
            }
        };

        $scope.$watch(function (scope) {
            return scope.model.adminPW
        }, function (phrase) {
            $http.post('/api/admin', phrase).then(function (response) {
                if (response.data['admin'] === 'true') {
                    $scope.model.adminOn = true;
                }
            });

        });


        $scope.dropDB = function () {
            alert("Drop issued, page will reload shortly!");
            RunpoolService.dropDB().then(function () {
                alert("DB has been dropped and recreated!");
                $scope.hardReset();
            });

        };


        $scope.makeDB = function () {
            alert("Create issued, page will reload shortly!");
            RunpoolService.makeDB().then(function () {
                alert('Database Created.');
                $scope.hardReset();
            });

        };

        $scope.updateDB = function () {
            alert("Update issued, page will reload shortly!");
            RunpoolService.updateDB().then(function () {
                alert('Database Updated.');
                $scope.hardReset();
            });

        };


        $scope.pushReference = function () {
            RunpoolService.pushReference($scope.model.rightRun, $scope.model.rightEntry);
        };


        $scope.image = function (url, caption) {
            var modalInstance = $modal.open({
                templateUrl: 'static/app/views/templates/image-modal.html',
                controller: 'ModalCtrl',
                resolve: {
                    data: function () {
                        return {
                            url: url,
                            caption: caption
                        }
                    }
                },
                size: 'lg'

            });

            modalInstance.result.then(function () {

            });
        };


        $scope.loadReference = function () {
            RunpoolService.fetchRefHists('', '').then(function (data) {
                $scope.model.newRight = $scope.model.rightHists = data.hists;
                $scope.applyFilters();
                $scope.model.rightStage = 'view';
                $scope.model.rightRunName = 'Reference';
                $scope.model.rightEntry = 'Reference';
            });
        };


        $scope.hardReset();


    });