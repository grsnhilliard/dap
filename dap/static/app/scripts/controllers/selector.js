'use strict';

/**
 * @ngdoc function
 * @name dqmApp.controller:SelectorCtrl
 * @description
 * # SelectorCtrl
 * Controller of the dqmApp
 */
angular.module('dqmApp')
    .controller('SelectorCtrl', function ($modal, $route, $scope, $window, $location, RunpoolService) {
        $scope.fetchRuns = function () {
            RunpoolService.fetchRuns().then(function (data) {
                $scope.model.runs = data.runs;
            });
        };


        $scope.newTab = function () {
            $window.open('http://lbnegpvm01.fnal.gov:5000/', '_blank');

        };

        $scope.admin = function () {
            $location.path('/admin');
        };

        $scope.swap = function () {
            if ($scope.model.eSwitch === 'events') {
                $scope.model.eSwitch = 'subRuns';

            } else if ($scope.model.eSwitch === 'subRuns') {
                $scope.model.eSwitch = 'events';
            }
            if ($scope.model.stage != 'runs' && $scope.model.stage != 'view') {
                $scope.model.stage = $scope.model.eSwitch;
                $scope.stageOne($scope.model.runName);
            }
            if ($scope.model.refStage != 'runs' && $scope.model.refStage != 'view') {
                $scope.model.refStage = $scope.model.eSwitch;
                $scope.stageOneRef($scope.model.refRun);

            }

        };
        $scope.leftReset = function () {
            $scope.model.stage = 'runs';
            $scope.model.runName = '';
            $scope.model.entry = '';
        };

        $scope.rightReset = function () {
            $scope.model.refStage = 'runs';
            $scope.model.refRun = '';
            $scope.model.refEntry = '';
        };

        $scope.hardReset = function () {
            $scope.model = {
                search: '',
                stage: 'runs',
                runName: '',
                entry: '',
                refRun: '',
                refEntry: '',
                filterValues: {},
                admin: '',
                refStage: 'runs',
                adminOn: false
            };
            if (!$scope.model.eSwitch) {
                $scope.model.eSwitch = 'subRuns';

            }
            $scope.fetchRuns();
        };

        $scope.softReset = function () {
            $scope.model.filterValues = {};
            $scope.applyFilters();
        };


        $scope.stageOne = function (runName) {
            $scope.model.runName = runName;
            if ($scope.model.eSwitch === 'events') {
                RunpoolService.fetchEvents(runName).then(function (data) {
                    $scope.model.events = data.events;
                    $scope.model.stage = 'events';
                });
            } else if ($scope.model.eSwitch === 'subRuns') {
                RunpoolService.fetchSubRuns(runName).then(function (data) {
                    $scope.model.subRuns = data.subRuns;
                    $scope.model.stage = 'subRuns';
                });
            }
            $scope.model.search = '';
        };

        $scope.stageTwoRef = function (data) {
            $scope.model.refEntry = data;
            $scope.fetchHists();
            $scope.fetchRefHists();
            $scope.fetchFilters();
            $scope.model.refStage = 'view';

        };

        $scope.stageTwo = function (data) {
            $scope.model.entry = data;
            $scope.fetchHists();
            $scope.fetchRefHists();
            $scope.fetchFilters();
            $scope.model.stage = 'view';

        };

        $scope.stageOneRef = function (runName) {
            $scope.model.refRun = runName;
            if ($scope.model.eSwitch === 'events') {
                RunpoolService.fetchEvents(runName).then(function (data) {
                    $scope.model.refEvents = data.events;
                    $scope.model.refStage = 'events';
                });
            } else if ($scope.model.eSwitch === 'subRuns') {
                RunpoolService.fetchSubRuns(runName).then(function (data) {
                    $scope.model.refSubRuns = data.subRuns;
                    $scope.model.refStage = 'subRuns';
                });
            }
            $scope.model.search = '';
        };

        $scope.stageTwoRef = function (data) {
            $scope.model.refEntry = data;
            RunpoolService.fetchRefHists($scope.model.refRun, $scope.model.refEntry).then(function (data) {
                $scope.model.refHists = data.hists;
                $scope.model.newRef = data.hists;
            });
            $scope.model.refStage = 'view';
        };


        $scope.hardReset();


        $scope.fetchHists = function () {
            RunpoolService.fetchHists($scope.model.runName, $scope.model.entry).then(function (data) {
                $scope.model.hists = data.hists;
                $scope.model.new = data.hists;
                //console.log($scope.model.new);

            });
        };

        $scope.fetchRefHists = function () {
            RunpoolService.fetchRefHists($scope.model.refRun, $scope.model.refEntry).then(function (data) {
                $scope.model.refHists = data.hists;
                $scope.model.newRef = data.hists;
            });
        };

        $scope.fetchFilters = function () {
            RunpoolService.fetchFilters($scope.model.runName, $scope.model.entry).then(function (data) {
                $scope.model.filters = data.filters;
            });
        };
        $scope.loadReference = function () {
            RunpoolService.fetchRefHists('', '').then(function (data) {
                $scope.model.newRef = $scope.model.refHists = data.hists;
                $scope.applyFilters();
                $scope.model.refStage = 'view';
                $scope.model.refRun = 'Reference';
                $scope.model.refEntry= 'Reference';
            });
        };
        $scope.applyFilters = function () {
            $scope.model.new = [];
            $scope.model.newRef = [];

            console.log($scope.model.filterValues);
            if (!$scope.model.filterValues) {
                $scope.model.new = $scope.model.hists;
                $scope.model.newRef = $scope.model.refHists;
                return
            }

            for (var index in $scope.model.hists) {
                var histogram = $scope.model.hists[index];
                var flag = true;
                for (var category in $scope.model.filterValues) {
                    if (histogram.tags[category]) {
                        if (histogram.tags[category] === $scope.model.filterValues[category]) {

                        }
                        else {
                            flag = false;
                        }
                    }
                    else {
                        flag = false;
                    }
                }
                if (flag === true) {
                    $scope.model.new.push(histogram);
                }
            }


            for (index in $scope.model.refHists) {
                histogram = $scope.model.refHists[index];
                flag = true;
                for (category in $scope.model.filterValues) {
                    if (histogram.tags[category]) {
                        if (histogram.tags[category] === $scope.model.filterValues[category]) {

                        }
                        else {
                            flag = false;
                        }
                    }
                    else {
                        flag = false;
                    }
                }
                if (flag === true) {
                    $scope.model.newRef.push(histogram);
                }
            }
        };

        $scope.toggleFilter = function (key, value) {
            if ($scope.model.filterValues[key] === value) {
                delete $scope.model.filterValues[key];
            } else {
                $scope.model.filterValues[key] = value;
            }
            //else if ($scope.model.filterValues[key] === undefined) {
            //    $scope.model.filterValues[key] = value;
            //}
            console.log($scope.model.filterValues);
            $scope.applyFilters();
        };

        $scope.$watch(function (scope) {
            return scope.model.adminPW
        }, function (phrase) {
            if (phrase === 'password') {
                $scope.model.adminOn = true;
            }
        });


        $scope.dropDB = function () {
            alert("Drop issued, page will reload shortly!");
            RunpoolService.dropDB().then(function () {
                alert("DB has been dropped and recreated!");
                $scope.hardReset();
            });

        };

        $scope.makeDB = function () {
            alert("Update issued, page will reload shortly!");
            RunpoolService.makeDB().then(function () {
                alert('Database Updated.');
                $scope.hardReset();
            });

        };

        $scope.pushReference = function () {
            RunpoolService.pushReference($scope.model.refRun, $scope.model.refEntry);
        };


        $scope.image = function (url) {
            var modalInstance = $modal.open({
                templateUrl: 'static/app/views/templates/image-modal.html',
                controller: 'ModalCtrl',
                resolve: {
                    data: function () {
                        return {
                            url: url
                        }
                    }
                },
                size: 'lg'

            });

            modalInstance.result.then(function () {

            });
        };

    });
