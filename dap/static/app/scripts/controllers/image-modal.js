/**
 * Created by wghilliard on 8/7/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name dqmApp.controller:SelectorCtrl
 * @description
 * # SelectorCtrl
 * Controller of the dqmApp
 */
angular.module('dqmApp')
    .controller('ModalCtrl', function ($modalInstance, $scope, data) {

        $scope.cancel = function(){
            $modalInstance.dismiss();
        };

        $scope.url = data.url;
        $scope.caption = data.caption;

    });