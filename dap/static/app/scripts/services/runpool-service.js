'use strict';

/**
 * @ngdoc service
 * @name dqmApp.RunpoolService
 * @description
 * # RunpoolService
 * Service in the dqmApp.
 */
angular.module('dqmApp')
    .factory('RunpoolService', function RunpoolService($http) {

        var fetchRuns = function () {
            return $http.get('/api/runs').then(function (response) {
                // console.log(response.data);
                return response.data;
            });
        };

        var fetchSubRuns = function (runName) {
            return $http.get('/api/runs/' + runName).then(function (response) {
                // console.log(response.data);
                return response.data;
            });
        };

        var fetchEvents = function (runName) {
            return $http.get('/api/runs/' + runName).then(function (response) {
                // console.log(response.data);
                return response.data;
            });
        };

        var fetchFilters = function (runName, entry) {
            return $http.get('/api/filters/' + runName + '/' + entry).then(function (response) {
                // console.log(response.data);
                return response.data;
            });
        };

        var fetchHists = function (runName, entry) {
            return $http.get('/api/hists/' + runName + '/' + entry).then(function (response) {
                // console.log(response.data);
                return response.data;
            });
        };

        var fetchRefHists = function (runName, entry) {
            if (runName != '' && entry !='') {
                return $http.get('/api/refhists/' + runName + '/' + entry).then(function (response) {
                    // console.log(response.data);
                    return response.data;
                });
            }else{
                return $http.get('/api/refhists').then(function (response) {
                     //console.log(response.data);
                    return response.data;
                });
            }
        };

        var dropDB = function () {
            return $http.get('/api/drop').then(function (response) {
                 //console.log(response.data);
                return response.data;
            });
        };

        var makeDB = function () {
            return $http.get('/api/make').then(function (response) {
                // console.log(response.data);
                return response.data;
            });
        };

        var updateDB = function () {
            return $http.get('/api/update').then(function (response) {
                 //console.log(response.data);
                return response.data;
            });
        };

        var pushReference = function (runName, entry) {
            return $http.get('/api/reference/' + runName + '/' + entry).then(function (response) {
                return response.data;
            });
        };

        return {
            fetchRuns: fetchRuns,
            fetchEvents: fetchEvents,
            fetchSubRuns: fetchSubRuns,
            fetchHists: fetchHists,
            fetchRefHists: fetchRefHists,
            fetchFilters: fetchFilters,
            dropDB: dropDB,
            makeDB: makeDB,
            updateDB: updateDB,
            pushReference: pushReference
        };

    });
