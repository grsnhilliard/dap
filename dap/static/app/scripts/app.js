'use strict';

/**
 * @ngdoc overview
 * @name dqmApp
 * @description
 * # dqmApp
 *
 * Main module of the application.
 */
angular
    .module('dqmApp', [
        'ngRoute',
        'mgcrea.ngStrap',
        'ui.bootstrap',
        'ui.bootstrap-slider',
        //'angular-bootstrap-select',
        'checklist-model'

    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/selector', {
                templateUrl: 'static/app/views/selector.html',
                controller: 'SelectorCtrl'
            })
            .when('/viewer', {
                templateUrl: 'static/app/views/test.html',
                controller: 'TestCtrl'
            })
            .otherwise({
                redirectTo: '/viewer'
            });

    }).config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }]);
