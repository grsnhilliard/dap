'use strict';

$(document).ready(function () {
    // MODAL
    var $run_select = $(this).find('[data-select_id="run"]');

    //$.ajax({
    //   type: 'GET',
    //    url: 'api/runs/'
    //});

    $.ajax({
        type: 'GET',
        url: '/api/runs',
        dataType: 'json',
        success: function (data) {
            console.log('success', data['runs']);

            $.each(data['runs'], function (i, run) {
                $run_select.append('<option>' + run['run_name'] + '</option>');
            });
        }
    });

    $run_select.on('click', function () {
        $.ajax({
            type: 'GET',
            url: '/api/runs/' + $(this).find(),
            dataType: 'json',
            success: function (data) {
                console.log('success', data['runs']);

                $.each(data['runs'], function (i, run) {
                    $run_select.append('<option>' + run.run_name + '</option>');
                });
            }
        });
    });

    var $modal_image = $(this).find('.modal_image');
    $('.image-hook').on('click', function () {
        var $original = $(this).find('.image');
        $modal_image.attr('src', $original.attr('src'));
    });

    // ISOTOPE
    var $container_left = $('#grid_left').isotope({
        layoutMode: 'fitRows',
        itemSelector: '.panel'
    });
    var filterFns = {
        'plane': '.z_plane, .u_plane, .v_plane'
    };

    var $container_right = $('#grid_right').isotope({
        layoutMode: 'fitRows',
        itemSelector: '.panel'
    });


    $('.sort-button').on('click', function () {
        var filterValue = $(this).attr('data-filter');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');

        }

    });

    var $toolbar = $(this).find('.btn-toolbar');

    $toolbar.on('click', function () {

        var filters = [];

        $(this).find('.active').each(function () {
            var filterValue = $(this).attr('data-filter');
            filters.push(filterFns[filterValue] || filterValue);
        });

        filters = filters.join('');
        console.log(filters);

        $container_left.isotope({filter: filters});
        $container_right.isotope({filter: filters});
    });

//    DEV

    //$('.show-all').on('click'), function () {
    //    $toolbar.find('active').each(function(){
    //        $(this).removeClass('active');
    //    });
    //    console.log('meow');
    //    $container.isotope({filter: '*'});
    //};
});

