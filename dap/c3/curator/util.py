from models import Histogram, Config
from mongoengine.errors import OperationError
import json, re
from config import PATH, PW


def setup(debug=False):
    conf = Config.objects.first()

    if not conf:
        Config(password=PW).save()

    with open(PATH + "/config.JSON") as data_file:
        data = json.load(data_file)

    sort_dict = dict(data)

    return sort_dict
