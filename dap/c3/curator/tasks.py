__author__ = 'William Hilliard'

import re
import os
from pymongo.errors import *
from models import *
from __init__ import app, path
from config import IGNORE, PATH


@app.task(name='tasks.master')
def master(debug, sort_dict):
    stats = {'added': 0, 'not_added': 0}
    # for item in sort_dict:
    #     print item

    if debug:
        print('path = {0}'.format(path))
        print "sort_dict[captions], {0}".format(sort_dict['captions'])

    config_object = Config.objects().first()

    added_items = dict(config_object.to_mongo())['added']

    if debug: print "key_list: \n {0}".format(sort_dict['sort_keys'])

    for directory in os.listdir(path):
        if directory not in IGNORE:
            try:
                tmp_directory = added_items[directory]
                stats['not_added'] += 1

            except KeyError:
                parse.delay(directory, os.path.join(PATH, directory), sort_dict)
                added_items[directory] = True
                stats['added'] += 1
    # print added_items
    config_object['added'] = added_items

    if config_object.save():
        return True
        # print stats


@app.task(name='tasks.parse')
def parse(directory, fullPath, sort_dict, debug=False):
    # files = os.listdir(fullPath)
    split = re.split('[R E S]', directory)

    # Don't judge me. I didn't create the naming scheme, I just have to interpret it.
    run = 'Run ' + split[1][2:]
    if 'vent' in split[2]:
        type = 'event'
        entry = 'Event ' + split[2][4:]
    elif 'ubrun' in split[2]:
        type = 'subRun'
        entry = 'Subrun ' + split[2][5:]
    else:
        return
    if debug:
        print "run = " + run
        print "entry = " + entry

    filter_tags = dict()

    test_list = list()

    for root, dirs, files in os.walk(fullPath):
        test_list.append([root, files])

    for item in test_list:
        parent = item[0].split('/')[-1]
        if parent == directory:
            parent = ''
        for filename in item[1]:

            if (filename not in IGNORE) and (filename[-4:] == '.png'):
                # filename_split = re.split('[_.]', filename)
                filename_split = filename[:-4].split('_')
                # Populate the histogram dictionary.
                hist_dict = dict()

                hist_dict.update({'filename': filename,
                                  'run': run,
                                  'entry': entry,
                                  'fullPath': os.path.join(fullPath, parent, filename),
                                  'tags': {}
                                  })

                tags = dict()
                for item in filename_split:
                    # if item != "png" and (item not in IGNORE):
                        value = ''
                        try:
                            value = sort_dict['sort_keys'][item]
                            tags[value] = item
                        except KeyError as e:
                            # tags[item] = True
                            print 'error {0}'.format(e)

                        if value != '':
                            try:
                                if item not in filter_tags[value]:
                                    filter_tags[value].append(item)
                            except KeyError:
                                filter_tags[value] = [item]
                hist_dict.update({'tags': tags.copy()})

                try:
                    hist_dict.update({'caption': sort_dict['captions'][filename[:-4]]})
                except KeyError:
                    pass

                hist_object = Histogram(**hist_dict).save()

                # Update Entry
                if hist_object:
                    try:
                        entryObject = Entry.objects(name=entry, run=run, type=type).modify(upsert=True, new=True,
                                                                                           add_to_set__histograms=[
                                                                                               hist_object.id])
                    except PyMongoError as e:
                        print e

                # Update Run
                if entryObject:

                    if type in 'event':
                        try:
                            runObject = Run.objects(name=run).modify(upsert=True, new=True,
                                                                     add_to_set__events=[entryObject.id])
                        except PyMongoError as e:
                            print e
                    elif type in 'subRun':
                        try:
                            runObject = Run.objects(name=run).modify(upsert=True, new=True,
                                                                     add_to_set__subRuns=[entryObject.id])
                        except PyMongoError as e:
                            print e
                            # print hist_dict
                # print filter_tags
                # Update Filter
                if entryObject:
                    try:
                        filterObject = Filter.objects(entry=entryObject.id, name=entryObject.name).modify(upsert=True,
                                                                                                          new=True,
                                                                                                          screen=
                                                                                                          filter_tags)
                    except PyMongoError as e:
                        print e

            else:
                print "skipping: {0}".format(filename)
                pass

    return True
