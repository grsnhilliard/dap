from curator.tasks import master

from curator.util import setup


def start(debug):
    sort_dict = setup(debug)

    master.delay(debug, sort_dict)
    # master(debug, sort_dict)

